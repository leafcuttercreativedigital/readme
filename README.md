# Project title

One Paragraph of project description goes here

## Solution architecture

For complex projects, describe the modules that are part of the project and their key workflows:

* Module 1
* Module 2
* Module 3

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
#for application environment
php 7.x
mysql 5.10+
redis

#for local development & builds
npm
node
docker
testcafe
```

### Installing

A step by step series of examples that tell you have to get a development env running

```
#for docker based projects, run the docker compose up command, application will be available as per details in docker-compose.yml
docker-compose up

#for laravel based projects without docker
#first, update your .env with the correct details, and then run the following commands
npm run dev
composer install
php artisan migrate --db:seed
php artisan serve
#your application should now be available, normally on localhost:8000

#for wordpress based projects without docker
#first, create a WordPress database, then import the database
mysql -u root -p dbname < sql/db.sql
#configure your wp-config.php file, then start your PHP server and you are good to go!
```

## Running the tests

Explain how to run the automated tests for this system

All projects should be configured with a default set of tests and pipeline installed using the `leafcutter` command

Tests can be executed locally using the `./bin/test.sh` command

All critical test cases should be written as end-to-end tests using TestCafe

## Deployment

Add additional notes about how to deploy this to staging and live environments

For Elastic Beanstalk projects, use `eb deploy <environment>` e.g.
`eb deploy development`
`eb deploy staging`
`eb deploy production`

For single VPS/shared account projects, use `git clone <reponame>`, and then manually setup and deploy the database e.g. `mysql -u root -p dbname < sql/db.sql`

Ensure that the .htaccess file or server environment prevents access to .git and other sensitive files.

## Data migration

Outline any steps, commands or processes involved in transferring data from the old system to the new system

For Laravel based projects, running the seeds included with the migration should be sufficient

For WordPress based projects, you will need to manually copy the database files from one environment to the other

## Built With

* [Wordpress](https://wordpress.com/) - Website platform
* [Shopify](https://shopify.com/) - eCommerce platform
* [Laravel](https://laravel.com/) - Web app framework
* [Vue.js](https://vuejs.org/) - Frontend framework
* [React.js](https://vuejs.org/) - Frontend framework
* [MySql](https://www.mysql.com/) - Database

## Contributing

Please read [The Leafcutter Developer's Guide to the Galaxy](https://docs.google.com/document/d/1-VKydpl_-1cr1lYH4ve4BUZH_FuDS-WZv5ym7tTgbKE/edit) for details on our code of conduct and standard development processes.

## Authors

* **Developer 1** - *Initial work* - [Leafcutter](https://leafcutter.com.au)
* **Developer 2** - *Initial work* - [Leafcutter](https://leafcutter.com.au)

## License

This project is IP of [Leafcutter](http://leafcutter.com.au), and is not authorised for external use

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
